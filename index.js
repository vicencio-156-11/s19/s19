// alert('hi')

const getCube = 2 ** 3
	console.log(`The cube of 2 is ${getCube}`);


const address = {
	houseNum: '258',
	street: 'Washington Ave NW',
	country: 'California',
	postal: '90011'
};

const{houseNum, street, country,postal} = address
console.log (`I live at ${houseNum} ${street}, ${country} ${postal}`)

const animal ={
	animalName: 'Lolong',
	animalType: 'saltwater crocodile',
	animalWeight: '1075 kgs',
	animalLength: '20 ft 3 in'

}

const{animalName, animalType, animalWeight,animalLength} = animal
console.log (`${animalName} was a ${animalType}. He weighed at ${animalWeight} with a measurement of ${animalLength}`)



let number= [1,2,3,4,5]


number.forEach((number) => {
	console.log(parseInt(`${number}`))
});


const add = (a,b,c,d,e) => a + b + c + d + e

let reduceNumber = add(1,2,3,4,5)
console.log(reduceNumber)


class Dog {
	constructor (name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}


// creating an instance

const myDog = new Dog(); 

myDog.name = 'Frankie';
myDog.age  = 5
myDog.breed = 'Miniature Dachshund'

console.log(myDog);